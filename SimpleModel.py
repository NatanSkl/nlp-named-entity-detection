import numpy as np
from sklearn import svm
from gensim import downloader
import pickle
from sklearn import metrics


WORD_2_VEC_PATH = 'word2vec-google-news-300'
GLOVE_PATH = 'glove-twitter-200'


class SimpleModel:
    def __init__(self, embeddings_path, train_path):
        self.vocabulary_dict = {}  # { word: (embedded_word, tag), word2: (embedded_word2, tag2), ... }
        self.train_path = train_path
        self.sentences = []
        self.embeddings_path = embeddings_path
        print("start")
        self.embeddings = downloader.load(self.embeddings_path)
        print("end")

        self.model = svm.SVC(C=2)
        self.preprocess()

    def preprocess(self):
        with open(self.train_path) as f:
            words = []  # words[i] = (w, embedded_w) where embedded_w = glove[w]
            tags = []
            sentence = []
            for line in f:
                if line != '\n':
                    word, tag = line.split('\t')
                    sentence.append(word.lower())
                    tags.append(tag.split('\n')[0])
                    if word.lower() not in self.embeddings.key_to_index:
                        word_embd = np.zeros(200)
                    else:
                        word_embd = self.embeddings[word.lower()]
                    words.append((word.lower(), word_embd))
                else:
                    self.sentences.append(sentence)
                    sentence = []
        for i in range(len(words)):
            final_embd = words[i][1]
            if tags[i] != 'O':
                tag = 'X'
            else:
                tag = 'O'
            self.vocabulary_dict[words[i][0]] = (final_embd, tag)

    def predict(self, test_path, is_tagged):  # the predicting process of the model
        with open(test_path, encoding="utf-8") as f:
            words = []
            sentences = []
            sentence = []
            for line in f:
                if line != '\n':
                    if is_tagged:  #does not matter, because we dont run this on comp
                        word = line.split("\t")[0].lower()
                    else:
                        word = line.split('\n')[0].lower()
                    if word not in self.vocabulary_dict.keys():
                        if word in self.embeddings.key_to_index:
                            word_embds = self.embeddings[word]
                        else:
                            word_embds = np.zeros(200)
                    else:
                        word_embds = self.vocabulary_dict[word][0]
                    words.append(word_embds)
                    sentence.append((word, word_embds))
                else:
                    sentences.append(sentence)
                    sentence = []
        print("start predict")
        return self.model.predict(words)




def calc_F1():

    pred_lst = []
    with open("pred_labels.txt") as f:
        for line in f:
            tag = line.split('\n')[0]
            pred_lst.append(tag)

    for i, label in enumerate(pred_lst):
        if label == "O":
            pred_lst[i] = 0
        else:
            pred_lst[i] = 1

    real_dev_tags = []
    with open("data/dev.tagged", encoding="utf-8") as f:
        for line in f:
            if line != '\n':
                word, tag = line.split("\t")
                tag = tag.split('\n')[0]
                real_dev_tags.append(tag)

    for i, tag in enumerate(real_dev_tags):
        if tag != 'O':
            real_dev_tags[i] = 1
        else:
            real_dev_tags[i] = 0

    print(metrics.f1_score(y_true=real_dev_tags, y_pred=pred_lst))
    print()


if __name__ == '__main__':
    simple = SimpleModel(GLOVE_PATH, "data/train.tagged")
    embedded_words = []
    labels = []
    for v in simple.vocabulary_dict.values():
        embedded_words.append(v[0])
        labels.append(v[1])
    simple.model.fit(embedded_words, labels)
    test_path = "data/dev.tagged"
    pred_labels = simple.predict(test_path, True)
    print("end predict\n\n")
    with open(r'pred_labels.txt', 'w') as fp:
        for item in pred_labels:
            # write each item on a new line
            fp.write("%s\n" % item)
        print('Done')
    with open('simple.pkl', 'wb') as f:
        pickle.dump(simple.model, f)

    #calc_F1()

