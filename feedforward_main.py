from torch.optim import Adam
from gensim import downloader
from feedforward_dataset import NN_Dataset
from feedforward_model import NerNN
from train import train
import numpy as np




GLOVE_PATH = 'glove-twitter-200'
train_path = "data/train.tagged"
test_path = "data/dev.tagged"


def load_glove():
    return downloader.load(GLOVE_PATH)


embeddings = load_glove()
train_ds = NN_Dataset(train_path, embeddings)
print('created train')
test_ds = NN_Dataset(test_path, embeddings)
datasets = {"train": train_ds, "test": test_ds}
model = NerNN(num_classes=len(train_ds.idx_to_tag.keys()), vocab_size=train_ds.vocabulary_size)
optimizer = Adam(params=model.parameters())
train(model=model, data_sets=datasets, optimizer=optimizer, num_epochs=15)

