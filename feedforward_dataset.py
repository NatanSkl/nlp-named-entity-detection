import torch
from torch.utils.data import Dataset
import numpy as np


class NN_Dataset(Dataset):

    def __init__(self, train_path, embeddings):
        self.train_path = train_path
        self.embeddings = embeddings
        self.words = []
        self.labels = []

        # preprocess
        with open(self.train_path, encoding="utf8") as f:
            # self.words[i] = (w, embedded_w) where embedded_w = glove[w]
            for line in f:
                if line != '\n':
                    word, label = line.split('\t')
                    if word.lower() not in self.embeddings.key_to_index:
                        word_embd = np.zeros(200)
                    else:
                        word_embd = self.embeddings[word.lower()]
                    self.words.append((word.lower(), word_embd))
                    binary_label = "O" if label.split('\n')[0] == "O" else "X"
                    self.labels.append(binary_label)  # remove \n

        self.tags_to_idx = {tag: idx for idx, tag in enumerate(sorted(list(set(self.labels))))}
        self.idx_to_tag = {idx: tag for tag, idx in self.tags_to_idx.items()}
        self.vocabulary_size = len(self.words[0][1])

    def __getitem__(self, item):
        cur_sen = self.words[item][1]  # get embedding
        cur_sen = torch.FloatTensor(cur_sen).squeeze()
        label = self.labels[item]
        label = self.tags_to_idx[label]
        # label = torch.Tensor(label)
        data = {"input_ids": cur_sen, "labels": label}
        return data

    def __len__(self):
        return len(self.words)